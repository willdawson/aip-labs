package au.edu.uts.aip.todo;

import java.util.*;

public class TaskList {

    private ArrayList<Task> tasks;

    public TaskList() {
        tasks = new ArrayList<>();
    }

    public List<Task> getTasks() {
        return tasks;
    }

    void removeTask(Task task) {
        tasks.remove(task);
    }

    void addTask(Task task) {
        tasks.add(task);
    }

}