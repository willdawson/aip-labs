package au.edu.uts.aip.todo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "TodoServlet", urlPatterns = {"/todo"})
public class TodoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        String command = request.getParameter("command");
        if (command == null) command = "";
        
        switch (command) {
            case "list":   getList(request, response, session);
                           break;
            case "create": getCreate(request, response, session);
                           break;
            case "add":    postAdd(request, response, session);
                           break;
            default:       getList(request, response, session);
        }   
        
    }
    
    private void getList(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws ServletException, IOException {
        
        TaskList taskList = (TaskList) session.getAttribute("taskList");
        if (taskList == null) {
            taskList = new TaskList();
            session.setAttribute("taskList", taskList);
        }
        
        request.getRequestDispatcher("/WEB-INF/views/list.jsp").forward(request, response);
        
    }
    
    private void getCreate(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws ServletException, IOException {
       
        request.getRequestDispatcher("/WEB-INF/views/create.jsp").forward(request, response);
        
    }
    
    private void postAdd(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws ServletException, IOException {
        
        String title = request.getParameter("title");
        String notes = request.getParameter("notes");
        
        if (!(title == null || title.equals(""))) {
            TaskList taskList = (TaskList) session.getAttribute("taskList");
            Task task = new Task();
            task.setTitle(title);
            task.setNotes(notes);
            task.setTaskList(taskList);

            session.setAttribute("taskList", taskList);
            session.setAttribute("task", task);
        }
        
        request.getRequestDispatcher("/WEB-INF/views/add.jsp").forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
