package au.edu.uts.aip.todo;

public class Task {
    private String title;
    private String notes;
    private TaskList taskList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public TaskList getTaskList() {
        return taskList;
    }

    public void setTaskList(TaskList newTaskList) {
        if (taskList != null) {
            taskList.removeTask(this);
        }
        taskList = newTaskList;
        taskList.addTask(this);
    }
}
