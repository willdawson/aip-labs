<h1>Create New To-Do Item</h1>

<form action="todo?command=add" method="POST" class="create">

    <div class="create__title">
        <label for="title">Title</label>
        <input type="text" name="title" />
    </div>

    <div class="create__notes">
        <label for="notes">Notes</label>
        <textarea type="text" name="notes" rows="4"></textarea>
    </div>

    <button type="submit">Submit Query</button>

</form>
