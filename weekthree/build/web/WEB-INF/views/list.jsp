<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>To-Do List</h1>
<p><b>Current To-Do Items</b></p>
<ul>
    <c:forEach var="task" items="${taskList.tasks}">
        <li>${task.title}: <em>${task.notes}</em></li>
    </c:forEach>
</ul>
<p><a href="todo?command=create">Create a new task</a></p>
