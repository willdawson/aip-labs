package au.edu.uts.aip.waitinglist;

import java.io.*;
import java.util.*;

public class WaitingListDatabase implements Serializable {

    // Helper to generate unique identifiers
    private static int idGenerator;
    private static synchronized int generateUniqueId() {
        idGenerator++;
        return idGenerator;
    }

    private static LinkedHashMap<Integer, Group> groups = new LinkedHashMap<>();

    public static Collection<Group> findAll() {
        return groups.values();
    }

    public static void create(Group group) {
        group.setId(generateUniqueId());
        groups.put(group.getId(), group);
    }

    public static Group read(int index) {
        return groups.get(index);
    }

    public static void update(Group group) {
        groups.put(group.getId(), group);
    }

    public static void delete(int index) {
        groups.remove(index);
    }

}