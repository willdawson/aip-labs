package au.edu.uts.aip.waitinglist;

import java.io.*;
import java.util.*;
import javax.enterprise.context.*;
import javax.faces.context.*;
import javax.inject.*;

@Named
@RequestScoped
public class WaitingListController implements Serializable {

    public Collection<Group> getGroups() {
        return WaitingListDatabase.findAll();
    }

}