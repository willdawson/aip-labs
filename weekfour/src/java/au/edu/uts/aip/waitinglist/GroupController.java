package au.edu.uts.aip.waitinglist;

import java.io.*;
import javax.enterprise.context.*;
import javax.inject.*;

@Named
@RequestScoped
public class GroupController implements Serializable {

    private Group group = new Group();

    public Group getGroup() {
        return group;
    }
    
    public String saveAsNew() {
        WaitingListDatabase.create(group);
        
        return "waitinglist";
    }
    
    public void loadGroup(int index) {
        group = WaitingListDatabase.read(index);
    }
    
    public String saveChanges() {
        WaitingListDatabase.update(group);
        
        return "waitinglist";
    }
    
    public String deleteGroup() {
        WaitingListDatabase.delete(group.getId());
        
        return "waitinglist";
    }

}