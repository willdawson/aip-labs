package au.edu.uts.aip.waitinglist;

import java.io.*;
import javax.validation.constraints.*;

public class Group implements Serializable {

    private int id;
    private String name;
    private int size;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Size(min=1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Min(1)
    @Max(20)
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}