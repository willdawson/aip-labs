package au.edu.uts.aip.advice;

import javax.inject.*;
import javax.enterprise.context.*;

@Named
@RequestScoped
public class QuestionController {

    public String doSuggestion() {
        if (Math.random() < 0.5) {
            return "morecats";
        } else {
            return "newgown";
        }
    }

}