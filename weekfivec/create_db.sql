-- User Details
create table account (
  username varchar(255) not null primary key,
  password varchar(255),
  fullname varchar(255) not null,
  email varchar(255) not null,
  dob date not null
);

-- Sample Data
insert into account (username, password, fullname, email, dob) values
  ('cbrady', 'password', 'Carol Brady', 'cbrady@example.com', {d '1934-02-14'});