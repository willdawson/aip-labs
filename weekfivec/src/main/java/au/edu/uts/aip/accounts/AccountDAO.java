package au.edu.uts.aip.accounts;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountDAO {

    public static final String JNDI_NAME = "jdbc/aip";

    private static final String SELECT_ACCOUNT =
            "select username, password, fullname, email, dob from account";
    private static final String ACCOUNT_ALL = SELECT_ACCOUNT;
    private static final String ACCOUNT_USERNAME = SELECT_ACCOUNT + " where username = ?";

    /**
     * Create a new account object based on the current result set.
     *
     * @param result
     * @return
     * @throws SQLException
     */
    private AccountDTO createDTO(ResultSet result) throws SQLException {
        AccountDTO account = new AccountDTO();
        account.setUsername(result.getString("username"));
        account.setEmail(result.getString("email"));
        account.setFullname(result.getString("fullname"));
        account.setPassword(result.getString("password"));
        account.setDob(result.getDate("dob"));
        return account;
    }

    /**
     * Find an account by username.
     *
     * @param username
     * @return
     */
    public AccountDTO find(String username) {
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);

            try (Connection connection = ds.getConnection();
                 PreparedStatement ps = connection.prepareStatement(ACCOUNT_USERNAME)) {

                ps.setString(1, username);

                try (ResultSet result = ps.executeQuery()) {
                    if (result.next()) return createDTO(result);
                }
            }
        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Find all accounts.
     *
     * @return
     */
    public ArrayList<AccountDTO> findAll() {
        ArrayList<AccountDTO> accounts = new ArrayList<>();

        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);

            try (Connection connection = ds.getConnection();
                 Statement statement = connection.createStatement();
                 ResultSet result = statement.executeQuery(ACCOUNT_ALL)) {

                while (result.next()) {
                    accounts.add(createDTO(result));
                }
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }

        return accounts;
    }

}
