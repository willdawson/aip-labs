package au.edu.uts.aip.accounts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@Named
@RequestScoped
public class AccountController {

    public ArrayList<AccountDTO> getAllUsers() throws NamingException {
        AccountDAO accountDAO = new AccountDAO();
        return accountDAO.findAll();
    }

    public void listUsers() throws NamingException {
        Logger log = Logger.getLogger(this.getClass().getName());
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/aip");

        try (Connection conn = ds.getConnection();
             Statement statement = conn.createStatement();
             ResultSet result = statement.executeQuery("select * from account")) {

            log.info("The accounts table contains:");

            ResultSetMetaData resultMeta = result.getMetaData();
            int numberOfColumns = resultMeta.getColumnCount();

            while (result.next()) {
                String output = "";
                log.info("Test");

                for (int i = 1; i <= numberOfColumns; i++) {
                    String columnName = resultMeta.getColumnName(i);
                    String columnValue = result.getString(i);
                    output += String.format("%s = %s", columnName, columnValue);
                    if (i != numberOfColumns) output += ", ";
                }

                log.info(output);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}