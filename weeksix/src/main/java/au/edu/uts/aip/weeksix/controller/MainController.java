package au.edu.uts.aip.weeksix.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Named
@RequestScoped
public class MainController {

    public void connect() throws SQLException, NamingException {
        DataSource dataSource = InitialContext.doLookup("jdbc/aip");
        Connection connection = dataSource.getConnection();
    }

}
