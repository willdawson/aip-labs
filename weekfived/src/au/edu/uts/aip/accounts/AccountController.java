package au.edu.uts.aip.accounts;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@RequestScoped
public class AccountController {
    public void listUsers() {
        try (Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/aip", "aip", "aip");
             Statement statement = conn.createStatement();
             ResultSet result = statement.executeQuery("select * from account")) {

            Logger log = Logger.getLogger(this.getClass().getName());
            log.info("The accounts table contains:");

            ResultSetMetaData resultMeta = result.getMetaData();
            int numberOfColumns = resultMeta.getColumnCount();

            while (result.next()) {
                String output = "";

                for (int i = 1; i <= numberOfColumns; i++) {
                    String columnName = resultMeta.getColumnName(i);
                    String columnValue = result.getString(i);
                    output += String.format("%s = %s", columnName, columnValue);
                    if (i != numberOfColumns) output += ", ";
                }

                log.info(output);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
        }




    }
}