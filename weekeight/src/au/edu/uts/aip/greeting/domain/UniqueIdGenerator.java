package au.edu.uts.aip.greeting.domain;

public class UniqueIdGenerator {

    private static int counter = 0;

    public static synchronized int generate() {
        counter++;
        return counter;
    }

}