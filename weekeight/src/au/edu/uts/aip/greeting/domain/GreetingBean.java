package au.edu.uts.aip.greeting.domain;

import javax.ejb.*;

@Stateless
public class GreetingBean {

    private int uniqueId = UniqueIdGenerator.generate();

    public int getUniqueId() {
        return uniqueId;
    }

    public String getGreeting() {
        return "Hello, World!";
    }

}