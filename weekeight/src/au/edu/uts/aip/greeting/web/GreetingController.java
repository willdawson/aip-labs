package au.edu.uts.aip.greeting.web;

import au.edu.uts.aip.greeting.domain.*;
import javax.ejb.*;
import javax.enterprise.context.*;
import javax.inject.*;

@Named
@RequestScoped
public class GreetingController {

    @EJB
    private GreetingBean greeting;

    public String getGreeting() {
        return greeting.getGreeting();
    }

    public int getUniqueId() {
        return greeting.getUniqueId();
    }

}